var divMovieList = document.querySelector(".movielist");
// View
const moviesView = ({movies}) => {
  test = `${movies.map(({imgSrc, name}) => (
                `<div class="movie-item-style-1">
                  <img src=" ${imgSrc}" alt="">
                    <div class="">
                    <h6>${name}</h6>
                    </div>
                  </div>	`
              )).join("")}
            `;
  return test;
}
//Model
function newList(){
  return newlist = {
    movies: [
        {
          name: null,
          imgSrc: null
        }
    ]
  };
}
const updateList = () => {
  divMovieList.innerHTML = moviesView(movielist);
}
popular();

function popular() {
  apiResponse("popular");
}
function trending() {
  apiResponse("trending");
}
function newest() {
  apiResponse("newest");
}
function rated() {
  apiResponse("rated");
}

function urlMaker(type,func){
  switch(func) {
    case "popular":
      path = "https://api.themoviedb.org/3/"+type+"/popular?api_key=1f553435fccc89dd1a3b2394c2cee7c3&language=en-US&total_results=20";
      return path;
    case "trending":
      path = "https://api.themoviedb.org/3/trending/"+type+"/week?api_key=1f553435fccc89dd1a3b2394c2cee7c3&total_results=20";
      return path;
    case "rated":
      path = "https://api.themoviedb.org/3/"+type+"/top_rated?api_key=1f553435fccc89dd1a3b2394c2cee7c3&language=en-US&total_results=20";
      return path;
    case "search":
      path = "https://api.themoviedb.org/3/search/"+type+"?api_key=1f553435fccc89dd1a3b2394c2cee7c3&language=en-US&query=";
      return path;
    case "newest":
        if(type == "movie"){
          tempval = "upcoming";
        }
        else{
          tempval = "on_the_air";
        }
        path = "https://api.themoviedb.org/3/"+type+"/"+tempval+"?api_key=1f553435fccc89dd1a3b2394c2cee7c3&language=en-US&total_results=20";
        return path;
    default:
      path = "https://api.themoviedb.org/3/"+type+"/popular?api_key=1f553435fccc89dd1a3b2394c2cee7c3&language=en-US&total_results=20";
      return path;
  }
}


function searchByName(){
  apiResponse("search");
}

function apiResponse(func){
  var typeList = document.querySelectorAll(".type");
  var type = typeList[0]["value"];
  if(func != "search"){
    var selected = document.querySelectorAll("li[type='selected']");
    selected[0].type = "notSelected";
    newSelVal = "."+func;
    var newSelected = document.querySelector(newSelVal);
    newSelected.parentElement.type = "selected";
  }
  var imgList = document.querySelectorAll("img");
  var movieNames = document.querySelectorAll("h6");
  let path = urlMaker(type,func);
  if(func == "search"){
    var query =  document.querySelector('#searchName');
    path = path+query.value;
  }
  fetch(path)
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    var results = data["results"];
    var int;
    movielist = newList();
    for (int in results) {
      posterPath = "https://image.tmdb.org/t/p/original"+results[int]["poster_path"];
      if(type == "tv"){
        movielist['movies'].push({"imgSrc":posterPath,"name":results[int]["original_name"]})
      }
      else{
      movielist['movies'].push({"imgSrc":posterPath,"name":results[int]["original_title"]})
    }
    }
    movielist.movies.splice(0,1);
    })
  .then(() =>{
    updateList();
  });
}


function showType(type){
  var typeList = document.querySelectorAll(".type");
  var type = typeList[0]["value"];
  var matches = document.querySelectorAll("li[type='selected']");
  var className = matches[0].firstElementChild.className;
  switch(className) {
    case "popular":
      popular();
      break;
    case "trending":
      trending();
      break;
    case "newest":
      newest();
      break;
    case "rated":
      rated();
      break;
  }
}